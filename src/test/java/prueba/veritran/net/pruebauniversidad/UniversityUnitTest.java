package prueba.veritran.net.pruebauniversidad;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UniversityUnitTest {
    @Test
    public void ceroTest() {
        assertEquals( "cero", MontoEscrito.getMontoEscrito(0));
    }

    @Test
    public void nueveTest() {
        assertEquals( "nueve", MontoEscrito.getMontoEscrito(9));
    }

    @Test
    public void milTest() {
        assertEquals("mil", MontoEscrito.getMontoEscrito(1000));
    }

    @Test
    public void mil_1_Test() {
        assertEquals("nueve mil ciento cincuenta y seis", MontoEscrito.getMontoEscrito(9156));
    }

    @Test
    public void millonTest() {
        assertEquals("un millon", MontoEscrito.getMontoEscrito(1000000));
    }

    @Test
    public void millon_1_Test() {
        assertEquals( "tres millones doscientos noventa mil seiscientos cuarenta y uno", MontoEscrito.getMontoEscrito(3290641));
    }

    @Test
    public void millon_2Test() {
        assertEquals("un millon cien mil", MontoEscrito.getMontoEscrito(1100000));
    }

    @Test
    public void millon_3Test() {
        assertEquals("seis millones trescientos cuarenta mil", MontoEscrito.getMontoEscrito(6340000));
    }

    @Test
    public void millon_4Test() {
        assertEquals("quinientos cincuenta y seis", MontoEscrito.getMontoEscrito(556));

    }

    @Test
    public void millon_5Test() {
        assertEquals("un millon", MontoEscrito.getMontoEscrito(1000000));
    }

    @Test
    public void millon_6Test() {
        assertEquals("ochocientos  millones quinientos cuarenta y seis mil", MontoEscrito.getMontoEscrito(800546000));
    }

    @Test
    public void veinteTest() {
        assertEquals( "veinticuatro", MontoEscrito.getMontoEscrito(24));
    }

    @Test
    public void cincoTest() {
        assertEquals( "setecientos mil", MontoEscrito.getMontoEscrito(700000));
    }

    @Test
    public void nueTest() {
        assertEquals( "novecientos mil", MontoEscrito.getMontoEscrito(900000));
    }

    @Test
    public void unoTest() {
        assertEquals( "diez", MontoEscrito.getMontoEscrito(10));
    }
    
}